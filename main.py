from flask import Flask, jsonify, request
import os

app = Flask(__name__)

@app.route('/')
def catch_one():
    return jsonify({"You are here: ": request.path})

@app.route('/<path:path>')
## you gotta catch them
def catch_all(path):
    return jsonify({"You are here: ": request.path})

if __name__ == "__main__":
    app.run()